﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Company.Model;
using Company.Services;

namespace Company
{
    class Program
    {
        #region Commandline Options
        /// <summary>
        /// Options class used for command line arguments
        /// </summary>
        /// <remarks>
        /// Internal to the program class because it should never be used elsewhere,
        /// and having it here makes it easier for maintance of the Main loop
        /// </remarks>
        class Options
        {
            [OptionList('i', "input", '|', Required = true, HelpText = "A '|' seperated list of complete paths to files for processing.")]
            public IEnumerable<string> InputFiles
            {
                get;
                set;
            }

            [Option('v', "verbose", DefaultValue = false, HelpText = "Print all debug messages to standard output.")]
            public bool Verbose
            {
                get;
                set;
            }

            [Option('d', "development", DefaultValue = false, HelpText = "This setting allows for the app to pause waiting for user input, instead of immediately exiting on completion or failure.")]
            public bool Development
            {
                get;
                set;
            }

            [ParserState]
            public IParserState LastParserState
            {
                get;
                set;
            }

            [HelpOption]
            public string GetUsage()
            {
                return HelpText.AutoBuild(this,
                    (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
            }
        }
        #endregion

        static int Main(string[] args)
        {
            var levelSwitch = new LoggingLevelSwitch();
            var log = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(levelSwitch)
                .WriteTo.LiterateConsole()
                .CreateLogger();

            Log.Logger = log;
            
            var whitespaceProcessor = new WhitespaceSeperatedProcessor(log);
            var poplulationService = new PopulationService(whitespaceProcessor, log);

            var options = new Options();
            if (Parser.Default.ParseArgumentsStrict(args, options))
            {
                // Configure the logging level based on what the user specified they wanted (as this isn't for production, just these two will do)
                levelSwitch.MinimumLevel = options.Verbose ? LogEventLevel.Verbose : LogEventLevel.Fatal;

                foreach (var input in options.InputFiles)
                {
                    var result = poplulationService.processFile(input);
                    if (result != null)
                    {
                        // We have a result, print it to the command window
                        Console.WriteLine(result.ToString());
                    }
                    else
                    {
                        log.Error("The file '{file}' failed for process successfully, see previous output for additonal details", input);
                    }
                }
            }

            if (options.Development)
            {
                DevelopmentWait();
            }

            return 0;
        }

        /// <summary>
        /// Wait for the user to press 'Enter' in the console
        /// </summary>
        /// <remarks>
        /// Split out for consistancy
        /// </remarks>
        private static void DevelopmentWait()
        {
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }
    }
}
