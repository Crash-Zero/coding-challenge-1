﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Core
{
    /// <summary>
    /// Provides a compatible class interface to System.Diagnostics.Contracts.
    /// </summary>
    /// <remarks>
    /// Shamelessly pulled from Microsoft, as changing the binaries to allow for
    /// its use out of diagnostics wouldn't be well conveyed in a txt file.
    /// </remarks>
    public static class Contract
    {
        /// <summary>
        /// Checks that the supplied condition is true, and if it is not, throws
        /// the supplied exception using the supplied message.
        /// </summary>
        /// <typeparam name="T">The type of exception to throw.</typeparam>
        /// <param name="condition">The condition that must be true.</param>
        /// <param name="message">The message for the exception if the condition is not true.</param>
        public static void Requires<T>(bool condition, string message) where T : Exception
        {
            if (!condition)
            {
                throw (T)Activator.CreateInstance(typeof(T), message);
            }
        }
    }
}
