﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Model
{
    /// <summary>
    /// This provides a strongly typed object for grades
    /// </summary>
    public enum Grade
    {
        Unknown = int.MinValue, 
        Kindergarden,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight
    }
}
