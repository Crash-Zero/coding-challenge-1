﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Model
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Convert the single char representation of a grade, into the strongly typed Grade object
        /// </summary>
        /// <param name="code">the char to convert</param>
        /// <returns>The grade the code represented or <c>Grade.Unknown</c></returns>
        public static Grade GetGradeFromCode(char code)
        {
            switch (code)
            {
                case 'k':
                case 'K':
                    return Grade.Kindergarden;
                case '1':
                    return Grade.One;
                case '2':
                    return Grade.Two;
                case '3':
                    return Grade.Three;
                case '4':
                    return Grade.Four;
                case '5':
                    return Grade.Five;
                case '6':
                    return Grade.Six;
                case '7':
                    return Grade.Seven;
                case '8':
                    return Grade.Eight;

                default:
                    return Grade.Unknown;
            }
        }

        /// <summary>
        /// Convert the offical two-letter abbreviations for provinces and territories of Canada into their enum representation.
        /// </summary>
        /// <param name="abbreviation">The offical two-letter abbrevation to convert.</param>
        /// <returns>The Province enum representation of the abbrevation, or Unknown if the passed in abbreviation isn't valid</returns>
        public static Province GetProvinceFromAbbreviation(string abbreviation)
        {
            switch (abbreviation.ToUpperInvariant())
            {
                case "AB":
                    return Province.Alberta;
                case "BC":
                    return Province.BritishColumbia;
                case "MB":
                    return Province.Manitoba;
                case "NB":
                    return Province.NewBrunswick;
                case "NL":
                    return Province.NewfoundlandAndLabrador;
                case "NS":
                    return Province.NovaScotia;
                case "NT":
                    return Province.NorthwestTerritories;
                case "NU":
                    return Province.Nunavut;
                case "ON":
                    return Province.Ontario;
                case "PE":
                    return Province.PrinceEdwardIsland;
                case "QC":
                    return Province.Quebec;
                case "SK":
                    return Province.Saskatchewan;

                default:
                    return Province.Unknown;
            }
        }

        public static string GetAbbreviationFromProvince(Province province)
        {
            switch (province)
            {
                case Province.Alberta:
                    return "AB";
                case Province.BritishColumbia:
                    return "BC";
                case Province.Manitoba:
                    return "MB";
                case Province.NewBrunswick:
                    return "NB";
                case Province.NewfoundlandAndLabrador:
                    return "NL";
                case Province.NovaScotia:
                    return "NS";
                case Province.NorthwestTerritories:
                    return "NT";
                case Province.Nunavut:
                    return "NU";
                case Province.Ontario:
                    return "ON";
                case Province.PrinceEdwardIsland:
                    return "PE";
                case Province.Quebec:
                    return "QC";
                case Province.Saskatchewan:
                    return "SK";
                    
                default:
                    return "Unknown";
            }
        }
    }
}
