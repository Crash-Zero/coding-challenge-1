﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Company.Model
{
    public class PopulationResult
    {
        internal Dictionary<string, long> aggregatedResult;

        public PopulationResult()
        {
            aggregatedResult = new Dictionary<string, long>();
        }

        public void AddNewRecord(PopulationRecord newResult)
        {
            if (!aggregatedResult.ContainsKey(newResult.City))
            {
                aggregatedResult.Add(newResult.City, 0L);
            }

            if (!aggregatedResult.ContainsKey(newResult.School))
            {
                aggregatedResult.Add(newResult.School, 0L);
            }

            if (!aggregatedResult.ContainsKey(ExtensionMethods.GetAbbreviationFromProvince(newResult.Province)))
            {
                aggregatedResult.Add(ExtensionMethods.GetAbbreviationFromProvince(newResult.Province), 0L);
            }
            
            aggregatedResult[newResult.City] = aggregatedResult[newResult.City] + newResult.Population;
            aggregatedResult[newResult.School] = aggregatedResult[newResult.School] + newResult.Population;
            aggregatedResult[ExtensionMethods.GetAbbreviationFromProvince(newResult.Province)] =
                aggregatedResult[ExtensionMethods.GetAbbreviationFromProvince(newResult.Province)] + newResult.Population;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            var first = aggregatedResult.Aggregate((max, cur) => max.Key.Length > cur.Key.Length ? max : cur).Key.Length;
            
            foreach (var result in aggregatedResult.OrderBy(x => x.Value))
            {
                sb.AppendLine(string.Format("{0}{1}", result.Key.PadRight(first + 3), result.Value));
            }

            return sb.ToString();
        }
    }
}
