﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Company.Core;

namespace Company.Model
{
    public class PopulationRecord : IEquatable<PopulationRecord>
    {
        /// <summary>
        /// Initilizes a new instance of the <see cref="PopulationRecord"/> class.
        /// </summary>
        /// <param name="province"></param>
        /// <param name="city"></param>
        /// <param name="school"></param>
        /// <param name="grade"></param>
        /// <param name="population"></param>
        /// <exception cref="ArgumentException">
        /// Thrown if <paramref name="province"/> is <c>Province.Unknown</c>, or if <paramref name="city"/>, <paramref name="school"/> are null or whitespace
        /// if <paramref name="population"/> is negative, or if <paramref name="grade"/> is <c>Grade.Unknown</c>
        /// </exception>
        public PopulationRecord(Province province, string city, string school, Grade grade, long population)
        {
            Contract.Requires<ArgumentException>(province!=Model.Province.Unknown, nameof(province));
            Contract.Requires<ArgumentException>(!string.IsNullOrWhiteSpace(city), nameof(city));
            Contract.Requires<ArgumentException>(!string.IsNullOrWhiteSpace(school), nameof(school));
            Contract.Requires<ArgumentException>(grade!=Grade.Unknown, nameof(grade));
            Contract.Requires<ArgumentException>(population > 0, nameof(population));

            Province = province;
            City = city;
            School = school;
            Grade = grade;
            Population = population;
        }

        public Province Province
        {
            get;
            private set;
        }

        public string City
        {
            get;
            private set;
        }

        public string School
        {
            get;
            private set;
        }

        public Grade Grade
        {
            get;
            private set;
        }

        public long Population
        {
            get;
            private set;
        }

        #region IEquatable<PopulationRecord>
        public bool Equals(object other)
        {
            return Equals(other as PopulationRecord);
        }

        public bool Equals(PopulationRecord other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Province == other.Province 
                && string.Equals(City, other.City)
                && string.Equals(School, other.School)
                && Grade == other.Grade
                && Population == other.Population;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Province.GetHashCode();
                hashCode = (hashCode * 31) + City.GetHashCode();
                hashCode = (hashCode * 31) + School.GetHashCode();
                hashCode = (hashCode * 31) + Grade.GetHashCode();
                hashCode = (hashCode * 31) + Population.GetHashCode();
                return hashCode;
            }
        }
        #endregion
    }
}
