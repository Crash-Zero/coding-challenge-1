﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Company.Core;
using Company.Model;
using Serilog;

namespace Company.Services
{
    /// <summary>
    /// The Whitespace seperated processor is an instance of IRawPopulationProcessor which can turn the whitespace seperated raw data
    /// into a population record
    /// </summary>
    public class WhitespaceSeperatedProcessor : IRawPopulationProcessor
    {
        private readonly ILogger _logger;

        public WhitespaceSeperatedProcessor(ILogger logger)
        {
            Contract.Requires<ArgumentNullException>(logger != null, nameof(logger));

            _logger = logger;
        }

        /// <inheritdoc />
        public PopulationRecord ProccessRawPopulationInformation(string rawInformation)
        {
            _logger.Information("About to start whitespace delimited processing of: '{rawInforamtion}'", rawInformation);
            Contract.Requires<ArgumentException>(!string.IsNullOrWhiteSpace(rawInformation), nameof(rawInformation));

            var parsed = rawInformation.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
            if (parsed.Length != 4)
            {
                // raw information passed in was not valid for this format, log and return null
                _logger.Error("Expected to find 4 columns worth of information, found only: '{columns}'", parsed.Length);
                return null;
            }

            var province = ExtensionMethods.GetProvinceFromAbbreviation(parsed[0]);
            if (Province.Unknown.Equals(province))
            {
                // The province could not be successfully determined
                _logger.Error("Expected to find a valid province , found: '{province}' instead", parsed[0]);
                return null;
            }

            var grade = ExtensionMethods.GetGradeFromCode(parsed[3][0]);
            if (Grade.Unknown.Equals(grade))
            {
                // The charector found that should denote a grade between kindergarden and grade 8 did not
                _logger.Error("Expected to find a valid grade indicator, found: '{grade}' instead of K, or 1-8", parsed[3][0]);
                return null;
            }

            var rawPopulation = parsed[3].Substring(1);
            long population = -1;
            if (!long.TryParse(rawPopulation, out population) || population <= 0)
            {
                // Unable to successfully determine the population for this record
                _logger.Error("Expected to find a population value, found: '{population}' instead", rawPopulation);
                return null;
            }
            
            return new PopulationRecord(province, parsed[1], parsed[2], grade, population);
        }
    }
}
