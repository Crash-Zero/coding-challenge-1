﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Company.Core;
using Company.Model;
using Serilog;

namespace Company.Services
{
    public class PopulationService
    {
        internal IRawPopulationProcessor _processor;
        private readonly ILogger _logger;

        /// <summary>
        /// Initilize a new instance of the <see cref="PopulationService"/> class.
        /// </summary>
        /// <param name="rawProcessor">The population processor</param>
        /// <exception cref="ArgumentNullException">Thrown if rawProcessor is the default IRawPopulationProcessor or logger is <c>null</c></exception>
        public PopulationService(IRawPopulationProcessor rawProcessor, ILogger logger)
        {
            Contract.Requires<ArgumentNullException>(rawProcessor != default(IRawPopulationProcessor), nameof(rawProcessor));
            Contract.Requires<ArgumentNullException>(logger != null, nameof(logger));

            _processor = rawProcessor;
            _logger = logger;
        }

        /// <summary>
        /// Process an input file into a single population result
        /// </summary>
        /// <param name="filePath">The complete path to the file to be processed</param>
        /// <returns>
        /// PopulationResult containing the aggregated information from the entire file
        /// </returns>
        public PopulationResult processFile(string filePath)
        {
            var aggregatedInformation = new PopulationResult();
            var fi = new FileInfo(filePath);
            if (!fi.Exists)
            {
                _logger.Error("Path specified was not a file!");
                return null;
            }

            using (var fileStream = File.OpenRead(filePath))
            {
                using (var reader = new StreamReader(fileStream, Encoding.UTF8, true, 128))
                {
                    string rawInfo;
                    while ((rawInfo = reader.ReadLine()) != null)
                    {
                        var record = _processor.ProccessRawPopulationInformation(rawInfo);
                        if (record != null)
                        {
                            aggregatedInformation.AddNewRecord(record);
                        }
                        else
                        {
                            _logger.Error("The line '{line}' failed to process successfully, see previous output for additional details", rawInfo);
                        }
                    }
                }
            }

            return aggregatedInformation;
        }
    }
}
