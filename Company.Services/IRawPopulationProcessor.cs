﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Company.Model;

namespace Company.Services
{
    /// <summary>
    /// This interface represents the ability to turn raw population information into a strongly typed populuation record
    /// </summary>
    public interface IRawPopulationProcessor
    {
        /// <summary>
        /// Turn the provided string, into the strongly typed Population Record type
        /// </summary>
        /// <param name="rawInformation">The raw population information to turn into a population record</param>
        /// <returns>A population record representing the raw information that was provided</returns>
        /// <exception cref="ArgumentException">Thrown if rawInformation is <c>null</c> or whitespace</exception>
        PopulationRecord ProccessRawPopulationInformation(string rawInformation);
    }
}
