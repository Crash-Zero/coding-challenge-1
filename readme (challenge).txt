Given the Sample Input (shown below), the file contains the columns: province,
city, school, grade (K-8) and population with brown eyes.  Column values are
separated by whitespace, except the grade and population.  In that case, the
first character is the grade �K� (for kindergarten) or a decimal digit value
for grades 1 through 8.  The remaining decimal digits represent the population
count for that grade.  The text data file is encoded in UTF-8 and field values
are in English.

Create a console program that reads the text data file and generates a report
summarizing the total population per province, city, and school.  Your code
must be written in C# and must show some common object oriented approaches.  
It should be commented as necessary to describe what is happening.  You may
only make a single pass through the data file (non-seek-able).  The data file
may also be larger than the system address space and therefore cannot be read
wholly into memory.  Writes to the console should be used to show the progress
as the file is processed and possibly diagnostics.

Sample Input:

ON        Waterloo    School1        K1000
ON        Waterloo    School1        12000
ON        Waterloo    School2        83000
ON        Kitchener   School3        K0043
ON        Kitchener   School3        10057
ON        Kitchener   School3        20003
ON        Kitchener   School3        30005
ON        Kitchener   School3        40000

Sample Output:School3       108 
Kitchener     108
School1      3000
School2      3000
Waterloo     6000
ON           6108