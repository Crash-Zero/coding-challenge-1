﻿using System;
using System.Linq;
using NSubstitute;
using Company.Model;
using Serilog;
using Xunit;

namespace Company.Services.Tests
{
    public class WhitespaceSeperatedProcessorUnitTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void ctor_validlogger_createsinstance()
        {
            // Arrange
            var valid_logger = Substitute.For<ILogger>();
            WhitespaceSeperatedProcessor sut = null;

            // Act
            var exception = Record.Exception(() => sut = new WhitespaceSeperatedProcessor(valid_logger));

            // Assert
            Assert.NotNull(sut);
            Assert.Null(exception);
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void ctor_nulllogger_throwsarguementnullexception()
        {
            // Arrange
            WhitespaceSeperatedProcessor sut = null;
            
            // Act
            var exception = Record.Exception(() => sut = new WhitespaceSeperatedProcessor(null));

            // Assert
            Assert.NotNull(exception);
            Assert.IsType<ArgumentNullException>(exception);
            Assert.Null(sut);
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void proccessRawPopulationInformation_validstringsinglespace_returnsPopulationRecord()
        {
            // Arrange
            var logger = Substitute.For<ILogger>();
            var sut = new WhitespaceSeperatedProcessor(logger);
            var valid_whitespace_record = "ON Waterloo School1 K1000";
            var expected_record = new PopulationRecord(Province.Ontario, "Waterloo", "School1", Grade.Kindergarden, 1000);
            PopulationRecord actual = null;

            // Act
            var exception = Record.Exception(() => actual = sut.ProccessRawPopulationInformation(valid_whitespace_record));

            // Assert
            Assert.Null(exception);
            Assert.NotNull(actual);
            Assert.Equal(expected_record, actual);
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void processRawPopulationInformation_validstringtabs_returnsPopulationRecord()
        {
            // Arrange
            var logger = Substitute.For<ILogger>();
            var sut = new WhitespaceSeperatedProcessor(logger);
            var valid_whitespace_record = "ON   Waterloo    School1     K1000";
            var expected_record = new PopulationRecord(Province.Ontario, "Waterloo", "School1", Grade.Kindergarden, 1000);
            PopulationRecord actual = null;

            // Act
            var exception = Record.Exception(() => actual = sut.ProccessRawPopulationInformation(valid_whitespace_record));

            // Assert
            Assert.Null(exception);
            Assert.NotNull(actual);
            Assert.Equal(expected_record, actual);
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void processRawPopulationInformation_validstringmultispace_returnsPopulationRecord()
        {
            // Arrange
            var logger = Substitute.For<ILogger>();
            var sut = new WhitespaceSeperatedProcessor(logger);
            var valid_whitespace_record = "ON   Waterloo                       School1                                             K1000";
            var expected_record = new PopulationRecord(Province.Ontario, "Waterloo", "School1", Grade.Kindergarden, 1000);
            PopulationRecord actual = null;

            // Act
            var exception = Record.Exception(() => actual = sut.ProccessRawPopulationInformation(valid_whitespace_record));

            // Assert
            Assert.Null(exception);
            Assert.NotNull(actual);
            Assert.Equal(expected_record, actual);
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void processRawPopulationInformation_wrongnumberofcolumns_returnsnull()
        {
            // Arrange
            var logger = Substitute.For<ILogger>();
            var sut = new WhitespaceSeperatedProcessor(logger);
            var wrongnumberofcolumns_record = "Waterloo School1 K1000";
            PopulationRecord actual = null;

            // Act
            var exception = Record.Exception(() => actual = sut.ProccessRawPopulationInformation(wrongnumberofcolumns_record));

            // Assert
            Assert.Null(exception);
            Assert.Null(actual);
            logger.Received(1).Error(Arg.Any<string>(), Arg.Any<int>());
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void processRawPopulationInformation_zeroPopulation_returnsnull()
        {
            // Arrange
            var logger = Substitute.For<ILogger>();
            var sut = new WhitespaceSeperatedProcessor(logger);
            var invalidPopulation_record = "ON Waterloo School1 1000";
            PopulationRecord actual = null;

            // Act
            var exception = Record.Exception(() => actual = sut.ProccessRawPopulationInformation(invalidPopulation_record));

            // Assert
            Assert.Null(exception);
            Assert.Null(actual);
            logger.Received(1).Error(Arg.Any<string>(), Arg.Any<string>());
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void processRawPopulationInformation_invalidProvince_returnsnull()
        {
            // Arrange
            var logger = Substitute.For<ILogger>();
            var sut = new WhitespaceSeperatedProcessor(logger);
            var invalidProvince_record = "BA Waterloo School1 K1000";
            PopulationRecord actual = null;

            // Act
            var exception = Record.Exception(() => actual = sut.ProccessRawPopulationInformation(invalidProvince_record));

            // Assert
            Assert.Null(exception);
            Assert.Null(actual);
            logger.Received(1).Error(Arg.Any<string>(), Arg.Any<string>());
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void processRawPopulationInformation_invalidPopulation_returnsnull()
        {
            // Arrange
            var logger = Substitute.For<ILogger>();
            var sut = new WhitespaceSeperatedProcessor(logger);
            var invalidPopulation_record = "ON Waterloo School1 KNotPop";
            PopulationRecord actual = null;

            // Act
            var exception = Record.Exception(() => actual = sut.ProccessRawPopulationInformation(invalidPopulation_record));

            // Assert
            Assert.Null(exception);
            Assert.Null(actual);
            logger.Received(1).Error(Arg.Any<string>(), Arg.Any<string>());
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void processRawPopulationInformation_invalidGrade_returnsnull()
        {
            // Arrange
            var logger = Substitute.For<ILogger>();
            var sut = new WhitespaceSeperatedProcessor(logger);
            var invalidGrade_record = "ON Waterloo School1 R1000";
            PopulationRecord actual = null;

            // Act
            var exception = Record.Exception(() => actual = sut.ProccessRawPopulationInformation(invalidGrade_record));

            // Assert
            Assert.Null(exception);
            Assert.Null(actual);
            logger.Received(1).Error(Arg.Any<string>(), Arg.Any<char>());
        }
    }
}
